<?php

namespace Finpaa\Sweden;

use Finpaa\Finpaa;

class Jubba
{
    private static $products;
    private static $ACCESS_TOKEN;

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();
        self::$products = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->JubaExpress()->Products()->default();
    }

    private static function getSequenceCode($name = 'products')
    {
        if(self::$$name) {
            return self::$$name;
        }
        else {
            self::selfConstruct();
            return self::getSequenceCode($name);
        }
    }

    private static function executeMethod($methodCode, $alterations, $returnPayload = false)
    {
        $response = Finpaa::executeTheMethod($methodCode, $alterations, $returnPayload);
        return array('error' => false, 'response' => json_decode(json_encode($response), true));
    }

    static function getAccessToken()
    {
        if(self::$ACCESS_TOKEN)
        {
            return self::$ACCESS_TOKEN;
        }
        else
        {
        	self::$ACCESS_TOKEN = 'Basic ' . base64_encode(env('JUBBA_USER_ID') . ":" . env('JUBBA_USER_PASSWORD'));
            return self::getAccessToken();
        }
    }

    static function registerCustomer($body)
    {
        $methodCode = self::getSequenceCode()->registerCustomer();

        $alterations[]['headers'] = array(
			'Accept' => null,
			'Authorization' => self::getAccessToken()
        );

        $body += array(
            "Telephone" => null,
            "Occupation" => null,
            "Remarks" => null,
            "State" => null,
            "PlaceOfBirth" => null,
            "Nationality" => null,
            "IsBeneficiary" => false,
        );

        $alterations[]['body'] = $body;

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['Data']))
            return $response['response'];

        return $response;
    }

    static function registerBeneficiary($body)
    {
        $methodCode = self::getSequenceCode()->resiagterBeneficiary();

        $alterations[]['headers'] = array(
			'Accept' => null,
			'Authorization' => self::getAccessToken()
        );

        $body += array(
            "Telephone" => null,
            "Occupation" => null,
            "Remarks" => null,
            "State" => null,
            "PlaceOfBirth" => null,
            "Nationality" => null,
            "IsBeneficiary" => true,
        );

        $alterations[]['body'] = $body;

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['Data']))
            return $response['response'];

        return $response;
    }

    static function getAgents($country, $city = null)
    {
        $methodCode = self::getSequenceCode()->listNominatedAgents();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken()
        );

        $alterations[]['body'] = array(
            "Country" =>  $country,
            "City" =>  $city
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['Data']))
            return $response['response'];

        return $response;
    }

    static function getExchangeRate($currencyCode)
    {
        $methodCode = self::getSequenceCode()->exchangeRate();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken()
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['Data']))
        {

            $rate = array();
            foreach ($response['response']['Data'] as $item)
            {
                if($item['CurrencyCode'] == $currencyCode)
                {
                    $rate[$currencyCode] = $item['Rate'];
                    break;
                }
            }
            return $rate;
        }

        return $response;
    }

    static function sendRemittance($body)
    {
        $methodCode = self::getSequenceCode()->sendRemittance();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken()
        );

        $body += array(
            "CustomerDocuments" => [
                [
                    "DocumentType" => null,
                    "DocumentNumber" => null,
                    "IssueDate" => null,
                    "ExpiryDate" => null,
                    "Country" => null
                ]
            ],
            "BeneficiaryDocuments" => [
                [
                    "DocumentType" => null,
                    "DocumentNumber" => null,
                    "IssueDate" => null,
                    "ExpiryDate" => null,
                    "Country" => null
                ]
            ]
        );

        $alterations[]['body'] = $body;

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['Data']))
            return $response['response'];

        return $response;
    }

    static function getRemittanceStatus($transferId)
    {
        $methodCode = self::getSequenceCode()->sendRemittanceStatus();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken()
        );

        $alterations[]['body'] = array(
            "ReferenceNum" => $transferId,
            "FromDate" => null,
            "ToDate" => null,
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['Data'][0]))
            return $response['response']['Data'][0];

        return $response;
    }

    static function cancel($transferId, $reason = "Pelvo system cancellation")
    {
        $methodCode = self::getSequenceCode()->cancelTransaction();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken()
        );

        $alterations[]['body'] = array(
            "ReferenceNum" => $transferId,
            "Remarks" => $reason,
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['Response']))
            $response['response']['Response'];

        return $response;
    }
}
